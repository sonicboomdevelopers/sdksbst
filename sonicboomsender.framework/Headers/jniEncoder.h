//
//  jniEncoder.h
//  sonicboomlibrary
//
//  Created by 1-18 Golf on 22/07/19.
//  Copyright © 2019 1-18 Golf. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN
@class jniUtil;

@protocol MyEncoder

-(void)onNativeEncoderCallback:(long)mNativeEncoder;
-(void)onBufferCallback:(char*)buffer;

-(Byte*)onGetBuffer;
-(void)onFreeBuffer:(int)filledBytesSize buffer:(char*)buffer;
-(void)onSinToken:(int*)tokens tokenLen:(int)tokenLen;
-(void)onInitSenderCallback:(int)packageId typeId:(int)typeId;

@end

@interface jniEncoder : NSObject

@property(strong, nonatomic) jniUtil* util;
@property(strong, nonatomic) id <MyEncoder> enc;
//@property(nonatomic) long mNativeEncoder;

-(instancetype)init;
-(size_t) InitSVCallback:(bool)hasConnection companyId:(int)companyId companyType:(int)companyType packageId:(int)packageId validTypeId:(int)validTypeId;
-(void) InitSenderCallback:(int) packageId typeId:(int)typeId;
-(void) initSB:(NSObject*)thiz context_object:(NSObject*)context_object company:(NSString *)companyId app:(NSString *)appId developmentMode:(int)isDevelopmentMode validBandBit:(int)validBandBit mNativeEncoder:(long)mNativeEncoder;

-(void) startSB:(NSObject*)thiz sampleRate:(int)sampleRate bufferSize:(int)bufferSize;

-(void) encodeMsgSB:(NSObject*)thiz validBandBit:(int)validBandBit tokens:(int*)tokens tokenLen:(int)tokenLen;

-(void) encodeValidationSB:(NSObject*)thiz validBandBit:(int)validBandBit messageNum:(int)messageNum presetMsgNum:(NSString *)presetMsgNum extraNum:(NSString *)extraNum validationNum:(NSString *)validationNum expirationDate:(NSString *)expirationDate ;

-(void) encodeSB:(NSObject*)thiz validBandBit:(int)validBandBit messageType:(int)messageType clientNum:(int)clientNum messageMultiplier:(int)messageMultiplier messageNum:(int)messageNum partNum:(int)partNum totalParts:(int)totalParts;

-(void) encodeTextSB:(NSObject*)thiz validBandBit:(int)validBandBit messageType:(int)messageType clientNum:(int)clientNum tokens:(int*)tokens tokenLen:(int)tokenLen partNum:(int)partNum totalParts:(int)totalParts;

-(void) encodeMixSB:(NSObject*)thiz validBandBit:(int)validBandBit messageType:(int)messageType clientNum:(int)clientNum tokens:(int*)tokens tokenLen:(int)tokenLen messageNum:(int)messageNum;

-(void) encodeDateSB:(NSObject*)thiz validBandBit:(int)validBandBit messageType:(int)messageType clientNum:(int)clientNum messageMultiplier:(int)messageMultiplier messageNum:(int)messageNum date:(int)date month:(int)month year:(int)year;

-(void) encodeHarmonicSB:(NSObject*)thiz tokens:(int*)tokens tokenLen:(int)tokenLen nodesCount:(int)nodesCount;

-(void) stopSB:(NSObject*)thiz;

-(void) uninitSB:(NSObject*)thiz;
//
-(int) getMaxEncoderIndex:(NSObject*)thiz;

@end


NS_ASSUME_NONNULL_END
